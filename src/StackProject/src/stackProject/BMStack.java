package calculator;

public class BMStack <E>
{

	//Attributes
	String[] al;
	int size;
	int arraytotal;
	
	public BMStack()
	{
		size =0;
		arraytotal = 0;
		al = new String[20];
	}
	public int getSize()
	{
		return size;
	}
	public String peek()
	{
		return al[size-1];			
	}
	public boolean empty()
	{
		if (size == 0)
			return true;
		else
			return false;
	}
	public void push(String s)
	{
		size ++; 
		al[size -1] = s;	
	}
	public String pop()
	{
		if(!empty())
		{
			size --;
			return al[size];
		}
		else
			return null;
	}
	
}
