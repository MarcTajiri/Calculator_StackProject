package stackProject;
import java.util.Stack;
import java.util.StringTokenizer;

public class Converter {
	private Stack<String> converterStack;
	private String postFixString;

	/**
	 * Default Constructor initializes the Stack to be used.
	 */
	public Converter() {
		converterStack = new Stack<String>();
	}


	/**
	 * Takes in an Infix expression, and returns a postfix expression as a string.
	 * @param infixString The Infix Expression, as a String.
	 * @return A Postfix expression, as a String.
	 */
	public String convertInfixToPostfix(String infixString) {
		postFixString = "";
		StringTokenizer st = new StringTokenizer(infixString);

		while (st.hasMoreTokens()) {
			checkToken(st.nextToken());
		}

		while(!converterStack.empty()) {
			postFixString += " " + converterStack.pop();
		}

		return postFixString;	
	}

	/**
	 * Takes in a single token unit from the infix string, and decides how to handle it.
	 * Either appends it to the postfix string, or passes it to the operand logic for further parsing.
	 * @param token The token to be parsed.
	 */
	private void checkToken (String token) { 
		if (token.equals("+") || token.equals("-") ||
				token.equals("*") || token.equals("/") ||
				token.equals("(") || token.equals(")")) 
		{
			parseOperand(token);
		}
		else postFixString += " " + token;

	}

	/**
	 * Compares the operand token to any currently on the stack. If the current operand has lower priority
	 * than those currently on the stack, the operands currently on the stack can be added to the postfix expression.
	 * @param operand
	 */
	private void parseOperand (String operand) {
		if (operand.equals(")")) 
		{
			//Runs until it finds the corresponding opening bracket on the stack
			while (!converterStack.peek().equals("("))
			{
				postFixString += " " + converterStack.pop();
			}

			converterStack.pop(); //Removing the Paired bracket from the stack
		}
		else 
		{
			//If the Operand is an opening bracket, immediately just pushes it onto the stack.
			if (operand.equals("(")) converterStack.push(operand);			
			else 
			{
				//Assigns priorities to the incoming operand, and the top operand on the stack.
				String top = converterStack.peek();
				int operandPriority = getPriority(operand), stackPriority = getPriority(top);
				
				//In the event that the incoming operand is lower priority, pops operands until there are no more
				//to deal with immediately
				if (operandPriority < stackPriority) 
				{
					while (!converterStack.peek().equals("(") && !converterStack.empty())
					{
						postFixString += " " + converterStack.pop();
					}
				}
				else 
				{
					converterStack.push(operand); //The operand was equal or higher priority, and is added to the stack
				}
			}
		}


	}

	private int getPriority(String operand) {
		int priority;
		switch (operand) {
		case "+":
		case "-":
			priority = 1;
			break;
		case "*":
		case "/":
			priority = 2;
			break;
		default:
			priority = -1; //Stack is probably empty.
			break;
		}
		return priority;
	}
}
